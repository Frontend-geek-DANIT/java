import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rand = (int)(Math.random()*100);
        boolean isRight = false;
        ArrayList<String> numbers = new ArrayList<String>();
        System.out.println("What is your name?");
        String name = sc.nextLine();
        System.out.println("Let the game begin!");
        while (!isRight) {
            System.out.println("Write the number in console");
            int number = Integer.parseInt(sc.nextLine());
            numbers.add(Integer.toString(number));
            if (number == rand) {
                System.out.println(numbers.toArray().length == 1 ? "Congratulations, " + name + "!\nU tried " + (numbers.toArray().length + 1) + " time!" : "Congratulations, " + name + "!\nU tried " + (numbers.toArray().length + 1) + " times!");
                System.out.println("Check out ur attempts: " + numbers);
                isRight = true;
            } else if (number < rand) {
                System.out.println("Your number is too small. Please, try again");
            } else {
                System.out.println("Your number is too big. Please, try again");
            }
        }
    }
}