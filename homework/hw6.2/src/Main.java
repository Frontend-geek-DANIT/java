import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean isRight = false;
        int rowTarget = (int) (Math.random() * 5) + 1;
        int columnTarget = (int) (Math.random() * 5) + 1;
        char[][] array = {
                {'0', '1', '2', '3', '4', '5'},
                {'1', '-', '-', '-', '-', '-'},
                {'2', '-', '-', '-', '-', '-'},
                {'3', '-', '-', '-', '-', '-'},
                {'4', '-', '-', '-', '-', '-'},
                {'5', '-', '-', '-', '-', '-'}
        };
        System.out.println("All Set. Get ready to rumble!");
        while (!isRight) {
            System.out.println("Choose the row");
            int row = in.nextInt();
            while (row < 0 || row > 5) {
                System.out.println("Row out of range\nTry again");
                row = in.nextInt();
            }
            System.out.println("Choose the column");
            int column = in.nextInt();
            while (column < 0 || column > 5) {
                System.out.println("Column out of range\nTry again");
                column = in.nextInt();
            }
            if (row != rowTarget || column != columnTarget) {
                array[row][column] = '*';
            } else {
                isRight = true;
                array[row][column] = 'x';
                System.out.println("Congratulations! You won!");
            }
            printArray(array);
        }
    }

    public static void printArray(char[][] array) {
        for (char[] row : array) {
            System.out.println(Arrays.toString(row));
        }
    }
}
