import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean isRunning = true;

        Scanner in = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "do home work";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to courses; watch a film";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "do home work";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to courses; watch a film";
        scedule[5][0] = "Friday";
        scedule[5][1] = "do home work";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to courses; watch a film";

        while(isRunning){
            boolean isRight = false;
            System.out.println("Please, input the day of the week");
            String day = in.nextLine();
            for (int i = 0; i < scedule.length; i++) {
                String dayOfWeek = scedule[i][0];
                if(day.trim().equalsIgnoreCase(dayOfWeek)) {
                    System.out.println("Your tasks for " + dayOfWeek + ": " + scedule[i][1] );
                    isRight = true;
                }
            }
            if (day.trim().equalsIgnoreCase("exit")) {
                System.out.println("Thank you for using our program");
                isRight = true;
                isRunning = false;
            }
            if(!isRight){
                System.out.println("Sorry, I don't understand you, please try again");
            }
        }
    }
}