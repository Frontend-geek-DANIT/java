package Utils;

import Entities.Family;
import Entities.Human;
import Entities.Pet;

public class Main {
    public static void main(String[] args) {
        // Create pets
        Pet dog = new Pet("dog", "Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Pet cat = new Pet("cat", "Whiskers", 3, 50, new String[]{"jump", "purr"});

        // Create humans
        Human father = new Human("Vito", "Karleone", 1955);
        Human mother = new Human("Jane", "Karleone", 1958);

        // Create family
        Family family = new Family(mother, father);
        family.setPet(dog);

        // Create child
        Human child = new Human("Michael", "Karleone", 1977, 90, family, new String[][]{{"Monday", "Tennis"}, {"Friday", "Chess"}});
        family.addChild(child);

        // Display family members
        System.out.println(father);
        System.out.println(mother);
        System.out.println(child);

        // Call methods on child
        child.greetPet();
        child.describePet();

        // Family methods
        System.out.println("Family count: " + family.countFamily());
        System.out.println(family);

        // Remove child
        family.deleteChild(0);
        System.out.println("Family count after deletion: " + family.countFamily());
        System.out.println(family);
    }
}
