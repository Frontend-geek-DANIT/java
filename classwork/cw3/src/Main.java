import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String[] items = {"ant", "Apple", "Pineapple", "ant", "Grapes", "ant", "ant", "Watermelon", "Vodka"};
        ArrayList<String> arrayOfItems = new ArrayList<String>();
        for (String item : items) {
            if (!item.equals("ant") && !item.equals("Vodka")) {
                arrayOfItems.add(item);
            }
        }

        System.out.println(arrayOfItems);
    }
}