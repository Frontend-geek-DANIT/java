import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Hello World\nHow old are u?");
        String age = scanner.nextLine();
        if (Integer.parseInt(age) < 18) {
            System.out.println("You are too young");
        } else {
            int credits = (int)(Math.random() * 701) + 300;
            int amountOfAttempts = 5;
            int att = 0;
            boolean isNum = false;
            while (att < amountOfAttempts && !isNum) {
                int num  = (int)(Math.random() * 31) + 1;
                System.out.println("Try to guess the number");
                int guess = scanner.nextInt();
                if (guess == num) {
                    System.out.println("You won!!!");
                    isNum = true;
                } else {
                    System.out.println("You lost!!!");
                    att++;
                    if (att == amountOfAttempts) {
                        scanner.nextLine();

                        System.out.println("Now each attempt will cost 300 credits\nU have " + credits + " credits.\nWould u like to play via credits? Write y/n");
                        String answer = scanner.nextLine();
                        if (answer.equalsIgnoreCase("y")) {
                            if (credits >= 300) {
                                System.out.println("Okay, let's do it!");
                                amountOfAttempts++;
                                credits -= 300;
                            } else {
                                System.out.println("Sorry, you do not have enough credits!\nSee u next time!");
                            }
                        } else {
                            System.out.println("Thanks for game, see u!");
                        }
                    }
                }
            }
        }
    }
}
