import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello User\nWrite ur email");
        Scanner in = new Scanner(System.in);
        String email = in.nextLine();
        while(!email.contains("@") || !email.contains(".")) {
            System.out.println("Ur email doesn't contain '@' or '.'\nTry again!");
            email = in.nextLine();
        }
        System.out.println("Write ur password");
        String password = in.nextLine();
        while(password.length() < 7) {
            System.out.println("Ur password doesn't contain at least 7 characters\nTry again!");
            password = in.nextLine();
        }
        System.out.println("Ur email:\n" + email + "\nPassword:\n" + password);
    }
}
